
class SimpleCalculator:

    def __init__(self,a,b):
        self.a = a
        self.b = b

    def sum(self):
        if isinstance(self.a, int) and isinstance(self.b, int):
            return(self.a+self.b)
        else:
            return "ERROR"

    def substract(self):
        if isinstance(self.a, int) and isinstance(self.b, int):
            return(self.a-self.b)
        else:
            return "ERROR"

    def multiply(self):
        if isinstance(self.a, int) and isinstance(self.b, int):
            return(self.a*self.b)
        else:
            return "ERROR"

    def divide(self):
        if isinstance(self.a, int) and isinstance(self.b, int):
            return(self.a/self.b)
        elif self.b == 0:
            raise ZeroDivisionError("Cannot divide by zero")
        else:
            return "ERROR"







